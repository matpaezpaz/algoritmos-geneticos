function AlgoritmoGenetico (bounds, cantidad_de_individuos, numero_de_generaciones,funcion_fitness,cantidad_emparejamiento, cantidad_mutacion,cantidad_elite) {
	var poblacion = GenerarPoblacion(bounds,cantidad_de_individuos);
	var elite;
	var elites_total = [];
	var promedios = [];
	var poblaciones = [];
	for (var i = 0; i < numero_de_generaciones; i++) {
		var {poblacion,elites:elites_generacion,promedio} = Generacion(bounds, poblacion,funcion_fitness,cantidad_emparejamiento, cantidad_mutacion,cantidad_elite);
		elites_total.push(elites_generacion);
		promedios.push(promedio);
		poblaciones.push(poblacion);
	}
	return {elites:elites_total,poblacion,promedios,poblaciones}
}