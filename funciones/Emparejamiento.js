function Emparejamiento(poblacion) {
	var poblacion_mezclada = ShuffleArray(poblacion);
	var poblacion_emparejamiento_actual = [];
	for (var i = 0; i < poblacion_mezclada.length-1; i++) {
		let punto_corte = EnteroAleatorio(0,poblacion_mezclada[0].cromosomas.length);
		let padre = poblacion_mezclada[i];
		let madre = poblacion_mezclada[++i];
		let hijo1 = padre.cromosomas.filter( (e,i) => i <= punto_corte ).concat(madre.cromosomas.filter( (e,i) => i > punto_corte ));
		let hijo2 = madre.cromosomas.filter( (e,i) => i <= punto_corte ).concat(padre.cromosomas.filter( (e,i) => i > punto_corte ));
		poblacion_emparejamiento_actual.push(hijo1);
		poblacion_emparejamiento_actual.push(hijo2);
	}
	if (poblacion_mezclada.length % 2 != 0) {
		poblacion_emparejamiento_actual.push(poblacion_mezclada[poblacion_mezclada.length-1].cromosomas);
	}
	return poblacion_emparejamiento_actual;
}