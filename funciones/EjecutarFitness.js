function EjecutarFitness(poblacion, funcion_fitness) {
    if (!poblacion[0].cromosomas) {
        poblacion = poblacion.map( a => ({ fitness: 0, cromosomas: a }) );
    }
    var valores = [];
    let maximo, minimo;
    var individuoMaximo;
    let sumatoria = 0;
    for (var i = 0; i < poblacion.length; i++) {
        let fitness = funcion_fitness(...poblacion[i].cromosomas);
        poblacion[i].fitness = fitness;
        valores.push(poblacion[i]);
        if (!maximo || fitness > maximo.fitness) {
        	maximo = poblacion[i]
        }
        minimo = (!minimo || fitness < minimo.fitness)?poblacion[i]:minimo;
        sumatoria += fitness;
    }
    return {poblacion:valores, maximo, minimo,promedio: sumatoria/poblacion.length};
}