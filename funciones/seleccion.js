function Seleccion(poblacion, funcion_fitness, cantidad_a_seleccionar, cantidad_elite) {
    //ejecuta fitness población, que devuelve un objeto con los valores de fitness, el máximo y el mínimo
    if (!poblacion[0].cromosomas) {
        poblacion = poblacion.map( a => ({ fitness: 0, cromosomas: a }) );
    }
    var {poblacion, maximo, minimo} = EjecutarFitness(poblacion,funcion_fitness);
    var e = 0.01;

    var valores_ordenados = poblacion.sort( (a,b) => b.fitness-a.fitness );

    //toma sólo los valores de la cantidad_a_seleccionar
    if (cantidad_a_seleccionar) {
        valores_ordenados = valores_ordenados.splice(0, cantidad_a_seleccionar);
        minimo = valores_ordenados[valores_ordenados.length-1];
    } else {
        cantidad_a_seleccionar = poblacion.length
    }

    //ajusta los fitness con el mínimo y suma e
    var valores_fitness_ajustados = valores_ordenados.map( (individuo) => individuo.fitness - minimo.fitness + e );

    // suma de los valores fitness
    var suma_valores_fitness = valores_fitness_ajustados.reduce( (suma,individuo) => suma + individuo , 0);

    // calcula las probabilidades
    var probabilidades_poblacion = valores_fitness_ajustados.map( (individuo,index) => ({fitness:(individuo / suma_valores_fitness),index:index}));


    //crear el arreglo de acumulada
    var acumulada = [];
    probabilidades_poblacion.reduce( (total,actual,index) => acumulada[index] = total+actual.fitness , 0 );

    //crea el arreglo de los tiros
    var ptr = Math.random() / cantidad_a_seleccionar;
    var tiros = [...Array(cantidad_a_seleccionar).keys()].map( (element, index) => ptr + ( index / cantidad_a_seleccionar ) );

    //selecciona según los tiros
    var poblacion_seleccionada = [];
    var j = 0;
    for (var i = 0;  i < acumulada.length; i++) {
        for (j; tiros[j] < acumulada[i] && j < tiros.length; j++) {
            poblacion_seleccionada.push(valores_ordenados[probabilidades_poblacion[i].index]);
        }
    }
    return {poblacion_seleccionada,elites:valores_ordenados.splice(0, cantidad_elite)};
}