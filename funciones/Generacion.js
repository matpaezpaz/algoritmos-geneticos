function Generacion (bounds,poblacion,funcion_fitness,cantidad_emparejamiento, cantidad_mutacion,cantidad_elite) {

	var {poblacion_seleccionada, elites:elites_generacion} = Seleccion(poblacion,funcion_fitness,undefined,cantidad_elite);
	var poblacion_nueva = [...elites_generacion].map( x => x.cromosomas );
	var elite_copia = JSON.parse(JSON.stringify(elites_generacion));

	let poblacion_emparejada = Emparejamiento(poblacion_seleccionada.splice(0, cantidad_emparejamiento));
	poblacion_nueva = poblacion_nueva.concat(poblacion_emparejada);

	let poblacion_mutada = Mutacion(poblacion_seleccionada.splice(0, cantidad_mutacion),bounds);
	poblacion_nueva = poblacion_nueva.concat(poblacion_mutada);

	let resto = poblacion.length - poblacion_nueva.length;

	let poblacion_resto = Seleccion(poblacion,funcion_fitness,resto,cantidad_elite);
	poblacion_nueva = poblacion_nueva.concat(Array.from(poblacion_resto.poblacion_seleccionada, x=> x.cromosomas));

	var {poblacion:poblacion_nueva,promedio} = EjecutarFitness(poblacion_nueva, funcion_fitness);
	var elites_nuevo;
	if (cantidad_elite > 0) {
		var poblacion_ordenada = [...poblacion_nueva].sort( (a,b) => b.fitness-a.fitness );
		elites_nuevo =  JSON.parse(JSON.stringify([...poblacion_ordenada].splice(0, cantidad_elite)));
	}
	return {elites:elites_nuevo,poblacion:poblacion_nueva,promedio};
}