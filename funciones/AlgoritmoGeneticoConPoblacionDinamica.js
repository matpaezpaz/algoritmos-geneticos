//función que realiza el algoritmo genético
// ----Parámetros
// bounds: arreglo de objetos {min,max}. Un elemento por cada dimensión.
// cantidad_de_individuos: entero.
// numero_de_generaciones: entero.
// funcion_fitness: funcion fitness. Función.
// porcentaje_emparejamiento: flotante entre 0 y 1
// porcentaje_mutacion: flotante entre 0 y 1
// factor_crecimiento: flotante entre 0 y 1
// factor_decrecimiento: flotante entre 0 y 1
// ----Retorna
// elites: arreglo de elites
// poblacion: última población
// promedios:  arreglo de promedios
// poblaciones: arreglo de todas las poblaciones
function AlgoritmoGeneticoConPoblacionDinamica (bounds, cantidad_de_individuos, numero_de_generaciones,funcion_fitness,porcentaje_emparejamiento, porcentaje_mutacion,factor_crecimiento,factor_decrecimiento) {
  var poblacion = GenerarPoblacion(bounds,cantidad_de_individuos);
  var elite;
  var elites_total = [];
  var promedios = [];
  var poblaciones = [poblacion];
  for (var i = 0; i < numero_de_generaciones; i++) {
    var cantidad_mutacion = Math.ceil(porcentaje_mutacion * poblacion.length);
    var cantidad_emparejamiento = Math.ceil(porcentaje_emparejamiento * poblacion.length);
    if (cantidad_emparejamiento + cantidad_mutacion > poblacion.length) {
      cantidad_emparejamiento -= (cantidad_emparejamiento + cantidad_mutacion - poblacion.length);
    }
    var {poblacion,elites:elites_generacion,promedio} = Generacion(bounds, poblacion,funcion_fitness,cantidad_emparejamiento, cantidad_mutacion,0);
    promedios.push(promedio);
    var poblacion_ordenada = JSON.parse(JSON.stringify(poblacion.sort( (a,b) => b.fitness-a.fitness )));
    elite = (elite)?elite:poblacion_ordenada[0];
    elites_total.push(JSON.parse(JSON.stringify(poblacion_ordenada[0])));
    if (poblacion_ordenada[0].fitness > elite.fitness) {
      var cantidad_poblacion_nueva = poblacion.length 
        + ( poblacion.length 
          * ( factor_crecimiento 
            * ( numero_de_generaciones - i ) 
              * ( ( poblacion_ordenada[0].fitness - elites_total[elites_total.length-2].fitness ) / ( elite.fitness ) ) ) );
      var {poblacion_seleccionada:poblacion_a_agregar} = Seleccion(poblacion_ordenada,funcion_fitness,undefined,0);
      poblacion = poblacion.concat(poblacion_a_agregar.splice(0, (Math.ceil(cantidad_poblacion_nueva) - poblacion.length)));
    } else {
      var cantidad_poblacion_nueva = poblacion.length * ( 1 - factor_decrecimiento);
      poblacion = poblacion_ordenada.splice(0, Math.ceil(cantidad_poblacion_nueva));
    }
    poblaciones.push(poblacion);
  }
  return {elites:elites_total,poblacion,promedios,poblaciones}
}