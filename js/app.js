var app = angular.module('Ag', [])/**
* Algoritmos Genéticos Module
*
* Description
*/
app.filter('porcentaje', function() {
	return function(x) {
		return '% ' + (x*100);
	}
});
app.filter('hhmmss',function() {
	return function(x) {
		var pad = function(toPad,totalSize) {
			var splitted = String(toPad).split('.');
			var s = splitted[0];
			while (s.length < (totalSize || 2)) {s = "0" + s;}
			return s + (splitted[1]?'.'+splitted[1]:'');
		}
		var hours   = Math.floor((x /1000)/ 3600) % 24;
		var minutes = Math.floor((x /1000)/ 60) % 60;
		var seconds = (x / 1000) % 60;
		return pad(hours,2)+':'+pad(minutes)+':'+pad(seconds);
	}
})
app.controller('AgCtrl', ['$scope', function ($scope) {
	$scope.cantidad_poblacion = 100;
	$scope.numero_generaciones = 20;
	$scope.porcentaje_emparejamiento = 0;
	$scope.porcentaje_mutacion = 0;
	$scope.porcentaje_elite = 0;
	$scope.Math = window.Math;
	$scope.Number = window.Number;
	$scope.funciones = [
	{
		nombre: 'De Jong 1',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(){ return [...arguments].reduce( (ant,act,i) => ant + (i+1) * Math.pow(act, 2) , 0)}
	},
	{
		nombre: 'De Jong 1 Invertida',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(){ return -[...arguments].reduce( (ant,act,i) => ant + (i+1) * Math.pow(act, 2) , 0)}
	},
	{
		nombre: 'De Jong 2',
		bounds: [{min:-2.048,max:2.048},{min:-2.048,max:2.048}],
		funcion: function(x,y){ return 100 * Math.pow((Math.pow(x, 2) - y),2)+ Math.pow( (1 - x ), 2)}
	},
	{
		nombre: 'De Jong 2 Invertida',
		bounds: [{min:-2.048,max:2.048},{min:-2.048,max:2.048}],
		funcion: function(x,y){ return -(100 * Math.pow((Math.pow(x, 2) - y),2)+ Math.pow( (1 - x ), 2))}
	},
	{
		nombre: 'De Jong 3',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(x,y){ return Math.floor(x) + Math.floor(y)}
	},
	{
		nombre: 'De Jong 3 Invertida',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(x,y){ return -(Math.floor(x) + Math.floor(y))}
	},
	{
		nombre: 'De Jong 4',
		bounds: [{min:-1.28,max:1.28},{min:-1.28,max:1.28}],
		funcion: function(x,y){ return [...arguments].reduce( (ant,act,i) => (ant + ((i+1) * Math.pow(act,4) + Math.random())), 0)}
	},
	{
		nombre: 'De Jong 4 Invertida',
		bounds: [{min:-1.28,max:1.28},{min:-1.28,max:1.28}],
		funcion: function(x,y){ return -[...arguments].reduce( (ant,act,i) => (ant + ((i+1) * Math.pow(act,4) + Math.random())), 0)}
	},
	{
		nombre: 'De Jong 5',
		bounds: [{min:-64,max:64},{min:-64,max:64}],
		funcion: function(x,y){ 
			var A = [[-32,-32,-32,-32,-32,-16,-16,-16,-16,-16,0,0,0,0,0,16,16,16,16,16,32,32,32,32,32],[-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32]];
			var z = 0;
			for (var j = 0; j < 5; j++) {
				for (var i = 0; i < 5; i++) {
					z = z + 0.002 + ( 1 / (( i + Math.pow(x - A[0][j*5+i], 6))+( j + Math.pow( y - A[1][j*5+i], 6))) );
				}
			}
			return z;
		}
	},
	{
		nombre: 'De Jong 5, 2',
		bounds: [{min:-50,max:50},{min:-50,max:50}],
		funcion: function(x,y){ 
			var A = [[-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32],[-32,-32,-32,-32,-32,-16,-16,-16,-16,-16,0,0,0,0,0,16,16,16,16,16,32,32,32,32,32]];
			var z = 0;
			for (var j = 0; j < 25; j++) {
				z += ( 1 / ( j+1 + Math.pow(x - A[0][j],6) + Math.pow(y - A[1][j], 6) ) );
			}
			return Math.pow(0.002 + z, -1);
		}
	},
	{
		nombre: 'De Jong 5, 2 invertida',
		bounds: [{min:-50,max:50},{min:-50,max:50}],
		funcion: function(x,y){ 
			var A = [[-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32,-32,-16,0,16,32],[-32,-32,-32,-32,-32,-16,-16,-16,-16,-16,0,0,0,0,0,16,16,16,16,16,32,32,32,32,32]];
			var z = 0;
			for (var j = 0; j < 25; j++) {
				z += ( 1 / ( j+1 + Math.pow(x - A[0][j],6) + Math.pow(y - A[1][j], 6) ) );
			}
			return -Math.pow(0.002 + z, -1);
		}
	},
	{
		nombre: 'De Jong 6',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(x,y){ return 10* arguments.length + [...arguments].reduce( (ant,act,i) => ant + ( Math.pow(act,2) - 10 * Math.cos(2*Math.PI*act) ) ,0)}
	},
	{
		nombre: 'De Jong 6 Invertida',
		bounds: [{min:-5.12,max:5.12},{min:-5.12,max:5.12}],
		funcion: function(x,y){ return -(10* arguments.length + [...arguments].reduce( (ant,act,i) => ant + ( Math.pow(act,2) - 10 * Math.cos(2*Math.PI*act) ) ,0))}
	},
	{
		nombre: ' X + Y',
		bounds: [{min:-10,max:10},{min:-10,max:10}],
		funcion: function(x,y){ return x + y}
	},
	{
		nombre: ' Schwefel',
		bounds: [{min:-500,max:500},{min:-500,max:500}],
		funcion: function(){
			var x = [...arguments];
			var resultado = 418.9829 * (x.length - 1);
			for (var i = 0; i < x.length; i++) {
				if (x[i] > 500 || x[i] < -500) {
					resultado += ( .02 * Math.pow(x[i],2) );
				} else {
					resultado -= x[i] * Math.sin(Math.sqrt(Math.abs(x[i])));
				}
			}
			return resultado;
		}
	},
	{
		nombre: ' Schaffer\'s F6',
		bounds: [{min:-10,max:10},{min:-10,max:10}],
		funcion: function(x,y){
			var numerador = Math.pow(Math.sin(Math.sqrt( x * x + y * y)), 2) - 0.5;
			var denominador = Math.pow( ( 1 + .001 * ( x * x + y * y ) ) , 2);
			return 0.5 + numerador / denominador;
		}
	}

	]
	$scope.funcion_elegida = $scope.funciones[0];
	$scope.funcion_fitness = $scope.funcion_elegida.funcion;
	$scope.bounds = $scope.funcion_elegida.bounds;
	$scope.cambiarFuncion = function() {
		$scope.funcion_fitness = $scope.funcion_elegida.funcion;
		$scope.bounds = $scope.funcion_elegida.bounds;
	}
	$scope.agregarBound = function(){
		$scope.bounds.push(angular.copy($scope.nuevoBound));
		//$scope.nuevoBound = {};
	}
	$scope.fitness = function(x,y) {
		return Math.pow(x,2) + Math.pow(y,2);
	}
	$scope.generarPoblacion = function() {
		$scope.poblacionGenerada = GenerarPoblacion($scope.bounds,$scope.cantidad_poblacion);
	}
	$scope.seleccionarPoblacion = function() {
		let {poblacion_seleccionada, elite} = Seleccion($scope.poblacionGenerada,$scope.funcion_fitness);
		$scope.poblacionSeleccionada = poblacion_seleccionada;
	}

	$scope.mostrarPoblacionGenerada = function(){
		$scope.tituloModal = "Poblacion Generada";
		$scope.individuos = [];
		$scope.individuos = angular.copy($scope.poblacionGenerada);
		//$scope.poblacionGenerada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}
	$scope.mostrarPoblacionSeleccionada = function(){
		$scope.tituloModal = "Poblacion Seleccionada";
		$scope.individuos = [];
		$scope.individuos = angular.copy($scope.poblacionSeleccionada);
		//$scope.poblacionSeleccionada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}
	$scope.mostrarPoblacionEmparejada = function(){
		$scope.tituloModal = "Poblacion Emparejada";
		$scope.individuos = [];
		$scope.individuos = angular.copy($scope.poblacionEmparejada);
		//$scope.poblacionSeleccionada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}
	$scope.mostrarPoblacionMutada = function(){
		$scope.tituloModal = "Poblacion Mutada";
		$scope.individuos = [];
		$scope.individuos = angular.copy($scope.poblacionMutada);
		//$scope.poblacionSeleccionada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}

	$scope.emparejarPoblacion = function() {
		$scope.poblacionEmparejada = Emparejamiento($scope.poblacionSeleccionada);
	}

	$scope.mutarPoblacion = function() {
		$scope.poblacionMutada = Mutacion($scope.poblacionSeleccionada,$scope.bounds);
	}

	$scope.ejecutarAlgoritmo = function() {
		$scope.cantidad_emparejamiento = $scope.Math.ceil($scope.cantidad_poblacion*$scope.porcentaje_emparejamiento);
		$scope.cantidad_mutacion = $scope.Math.ceil($scope.cantidad_poblacion*$scope.porcentaje_mutacion);
		$scope.cantidad_elite = $scope.Math.ceil($scope.cantidad_poblacion*$scope.porcentaje_elite);
		if ($scope.cantidad_emparejamiento+$scope.cantidad_mutacion+$scope.cantidad_elite>$scope.cantidad_poblacion) {
			$scope.cantidad_emparejamiento -= $scope.cantidad_emparejamiento+$scope.cantidad_mutacion+$scope.cantidad_elite-$scope.cantidad_poblacion;
		}
		let {elites,poblacion,promedios,poblaciones} = AlgoritmoGenetico ($scope.bounds
			, $scope.cantidad_poblacion, $scope.numero_generaciones
			,$scope.funcion_fitness,$scope.cantidad_emparejamiento, $scope.cantidad_mutacion,$scope.cantidad_elite);
		$scope.elites = elites;
		$scope.poblacion = poblacion;
		$scope.promedios = promedios;
		$scope.poblaciones = poblaciones;
		return {elites,poblacion,promedios,poblaciones};
	}
	$scope.ejecutarAlgoritmoPobDin = function() {
		let {elites,poblacion,promedios,poblaciones} = AlgoritmoGeneticoConPoblacionDinamica ($scope.bounds
			, $scope.cantidad_poblacion, $scope.numero_generaciones
			,$scope.funcion_fitness,$scope.porcentaje_emparejamiento, $scope.porcentaje_mutacion,$scope.factor_crecimiento,$scope.factor_decrecimiento);
		$scope.elites_agpd = elites;
		$scope.poblacion_agpd = poblacion;
		$scope.promedios_agpd = promedios;
		$scope.poblaciones_agpd = poblaciones;
		return {elites,poblacion,promedios,poblaciones};
	}

	$scope.ejecucionesMultiples = function() {
		let elites_comun_m = []
		,elites_agpd_m = []
		,poblacion_comun_m = []
		,poblacion_agpd_m = []
		,promedios_comun_m = []
		,promedios_agpd_m = []
		,poblaciones_comun_m = []
		,poblaciones_agpd_m = []
		;
		$scope.multiples = ($scope.multiples)?$scope.multiples:30;
		$scope.tiempo_comun = new Date();
		$scope.mejor_elite_comun = undefined;
		$scope.peor_elite_comun = undefined;
		for (var i = 0; i < $scope.multiples; i++) {
			let {elites:elites_comun,poblacion:poblacion_comun,promedios:promedios_comun,poblaciones:poblaciones_comun} = $scope.ejecutarAlgoritmo();
			elites_comun_m.push(elites_comun);
			poblacion_comun_m.push(poblacion_comun);
			promedios_comun_m.push(promedios_comun);
			poblaciones_comun_m.push(poblaciones_comun);
			let elite_actual = JSON.parse(JSON.stringify(elites_comun[elites_comun.length-1][0]));
			$scope.mejor_elite_comun = ($scope.mejor_elite_comun || elite_actual);
			$scope.mejor_elite_comun = ( $scope.mejor_elite_comun.fitness < elite_actual.fitness )?elite_actual:$scope.mejor_elite_comun;
			$scope.peor_elite_comun = ($scope.peor_elite_comun || elite_actual);
			$scope.peor_elite_comun = ( $scope.peor_elite_comun.fitness > elite_actual.fitness )?elite_actual:$scope.peor_elite_comun;
		}
		$scope.tiempo_comun = new Date() - $scope.tiempo_comun;
		$scope.mejor_elite_agpd = undefined;
		$scope.peor_elite_agpd = undefined;
		$scope.tiempo_agpd = new Date();
		for (var i = 0; i < $scope.multiples; i++) {
			let {elites:elites_agpd,poblacion:poblacion_agpd,promedios:promedios_agpd,poblaciones:poblaciones_agpd} = $scope.ejecutarAlgoritmoPobDin();
			elites_agpd_m.push(elites_agpd);
			poblacion_agpd_m.push(poblacion_agpd);
			promedios_agpd_m.push(promedios_agpd);
			poblaciones_agpd_m.push(poblaciones_agpd);
			let elite_actual = JSON.parse(JSON.stringify(elites_agpd[elites_agpd.length-1]));
			$scope.mejor_elite_agpd = ($scope.mejor_elite_agpd || elite_actual);
			$scope.mejor_elite_agpd = ( $scope.mejor_elite_agpd.fitness < elite_actual.fitness )?elite_actual:$scope.mejor_elite_agpd;
			$scope.peor_elite_agpd = ($scope.peor_elite_agpd || elite_actual);
			$scope.peor_elite_agpd = ( $scope.peor_elite_agpd.fitness > elite_actual.fitness )?elite_actual:$scope.peor_elite_agpd;
		}
		$scope.tiempo_agpd = new Date() - $scope.tiempo_agpd;

		$scope.elites_comun_m = elites_comun_m;
		$scope.elites_agpd_m = elites_agpd_m;
		$scope.poblacion_comun_m = poblacion_comun_m;
		$scope.poblacion_agpd_m = poblacion_agpd_m;
		$scope.promedios_comun_m = promedios_comun_m;
		$scope.promedios_agpd_m = promedios_agpd_m;
		$scope.poblaciones_comun_m = poblaciones_comun_m;
		$scope.poblaciones_agpd_m = poblaciones_agpd_m;
	}

	$scope.mostrarAlgoritmo = function(){
		$scope.tituloModal = "Algoritmo";
		$scope.individuos = [];
		$scope.individuos = $scope.poblacion;
		//$scope.poblacionSeleccionada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}
	$scope.mostrarAlgoritmoGPD = function(){
		$scope.tituloModal = "Algoritmo";
		$scope.individuos = [];
		$scope.individuos = $scope.poblacion_agpd;
		//$scope.poblacionSeleccionada.forEach( (e,i) => cm += "||| Individuo " + i + ": " + e );
	}


	$scope.graficaFuncion = function(){
		$scope.cantidad_de_puntos = ($scope.cantidad_de_puntos)?$scope.cantidad_de_puntos:41;
		cantidad_de_puntos = $scope.cantidad_de_puntos;
		let distancia_entre_limites_x = $scope.bounds[0].max - $scope.bounds[0].min;
		let espacio_entre_punto_x = distancia_entre_limites_x / cantidad_de_puntos;
		let distancia_entre_limites_y = $scope.bounds[1].max - $scope.bounds[1].min;
		let espacio_entre_punto_y = distancia_entre_limites_y / cantidad_de_puntos;
		let x = [...Array(cantidad_de_puntos).keys()].map( (e,i) => (e * espacio_entre_punto_x+ $scope.bounds[0].min) ).concat($scope.bounds[0].max);
		let y = [...Array(cantidad_de_puntos).keys()].map( (e,i) => (e * espacio_entre_punto_y+ $scope.bounds[1].min) ).concat($scope.bounds[1].max);
		let z = [];
		//y.forEach( e => z.push(x.map( (e,i) => (y[i]*y[i] + e*e))));
		for (var i = 0; i < y.length; i++) {
			z[i]=[];
			for (var j = 0; j < x.length; j++) {
				z[i][j] = $scope.funcion_fitness(x[j],y[i]);
			}
		}
		return {x,y,z};
	}

	$scope.plotear = function() {
		var {x,y,z} = $scope.graficaFuncion();
		let cantidad_elites = $scope.elites.length;
		let dist = 255 / cantidad_elites-1;
		let arreglo_color = [...Array(cantidad_elites).keys()].map( e => (e+1) * dist );
		let elites_x = [], elites_y = [], elites_z = [];
		$scope.elites.forEach( (e,i) => 
		{
			elites_x.push(e[0].cromosomas[0]);
			elites_y.push(e[0].cromosomas[1]);
			elites_z.push(e[0].fitness);
		});

		let linea_elites = {
			x:elites_x,
			y:elites_y,
			z:elites_z,
			opacity:1,
			name: 'Elites',
			mode:'lines+markers',
			text: $scope.elites.map( (e,i) => 'Generación ' + i),
			marker:{
				color: arreglo_color.map(e => 'rgb('+e+', '+e+', '+e+')'),
				size: 5,
				symbol: 'diamond-open',
				line: {
					color: arreglo_color.map(e => 'rgb('+e+', 0, '+(255-e)+')'),
					width: 1
				},
				opacity: 1
			},
			type:'scatter3d',
			line: {
				color: arreglo_color.map(e => 'rgb('+e+', 0, '+(255-e)+')'),
				width: 2,
			},
		}
		var data_z1 = [{
			z:z,
			x:x,
			y:y,
			type: 'surface',
			opacity: 0.7,
			name: $scope.funcion_elegida.nombre,
		}
		];
		data_z1 = data_z1.concat(linea_elites);


		var layout = {

			title: 'Funcion',
			margin: {
				t: 0, //top margin
				l: 0, //left margin
				r: 0, //right margin
				b: 0 //bottom margin
			},
			autosize:true,
			
		}

		var trace1 = {
			x: $scope.promedios.map( (e,i) => i),
			y: $scope.promedios.map( e => e),
			mode: 'lines+markers',
			type: 'scatter',
			text: $scope.promedios.map( (e,i) => 'G' + i),
			name: 'Promedios'
		};
		var trace2 = {
			name: 'Elites',
			x: $scope.elites.map( (e,i) => i),
			y: $scope.elites.map( e => e[0].fitness),
			mode: 'lines+markers',
			type: 'scatter',
			text: $scope.elites.map( (e,i) => 'G' + i + '||X:' + e[0].cromosomas[0] + '--Y:' + e[0].cromosomas[1] + 'F:' + $scope.funcion_fitness(...e[0].cromosomas))
		};


		Plotly.newPlot('grafica', data_z1, layout, {responsive: true});
		Plotly.newPlot('lineas', [trace1,trace2], {responsive: true});
	}

	$scope.plotearPD = function(cantidad_puntos) {
		var {x,y,z} = $scope.graficaFuncion(cantidad_puntos);
		let cantidad_elites = $scope.elites_agpd.length;
		let dist = 255 / cantidad_elites-1;
		let arreglo_color = [...Array(cantidad_elites).keys()].map( e => (e+1) * dist );
		let elites_x = [], elites_y = [], elites_z = [];
		$scope.elites_agpd.forEach( (e,i) => 
		{
			elites_x.push(e.cromosomas[0]);
			elites_y.push(e.cromosomas[1]);
			elites_z.push(e.fitness);
		});

		let linea_elites = {
			x:elites_x,
			y:elites_y,
			z:elites_z,
			opacity:1,
			name: 'Elites',
			mode:'lines+markers',
			text: $scope.elites_agpd.map( (e,i) => 'Generación ' + i),
			marker:{
				color: arreglo_color.map(e => 'rgb('+e+', '+e+', '+e+')'),
				size: 5,
				symbol: 'diamond-open',
				line: {
					color: arreglo_color.map(e => 'rgb('+e+', 0, '+(255-e)+')'),
					width: 1
				},
				opacity: 1
			},
			type:'scatter3d',
			line: {
				color: arreglo_color.map(e => 'rgb('+e+', 0, '+(255-e)+')'),
				width: 2,
			},
		}
		var data_z1 = [{
			z:z,
			x:x,
			y:y,
			type: 'surface',
			opacity: 0.7,
			name: $scope.funcion_elegida.nombre,
		}
		];
		data_z1 = data_z1.concat(linea_elites);
		var layout = {
			title: 'Funcion',
			margin: {
				t: 0, //top margin
				l: 0, //left margin
				r: 0, //right margin
				b: 0 //bottom margin
			},
			autosize:true,
			
		}

		var trace1 = $scope.createTrace('Promedios'
			,$scope.promedios_agpd.map( (e,i) => i)
			,$scope.promedios_agpd.map( e => e)
			,$scope.promedios_agpd.map( (e,i) => 'G' + i));

		var trace2 = $scope.createTrace('Elites'
			,$scope.elites_agpd.map( (e,i) => i)
			,$scope.elites_agpd.map( e => e.fitness)
			,$scope.elites_agpd.map( (e,i) => 'G' + i)
			);
		var trace3 = $scope.createTrace('Población'
			,$scope.poblaciones_agpd.map( (e,i) => i)
			,$scope.poblaciones_agpd.map( e => e.length)
			,$scope.poblaciones_agpd.map( (e,i) => 'G' + i)
			);
		Plotly.newPlot('graficapd', data_z1, layout, {responsive: true});
		Plotly.newPlot('lineaspd', [trace1,trace2], {responsive: true});
		Plotly.newPlot('poblacionespd', [trace3], {responsive: true});
	}


	$scope.createTrace = function(nombre,arregloX,arregloY,arregloText) {
		return {
			name:nombre,
			x:arregloX,
			y:arregloY,
			text:arregloText,
			mode: 'lines+markers',
			type: 'scatter',
		}
	}

	$scope.plotearMultiples = function() {
		var trace1 = $scope.createTrace('Promedios'
			,$scope.promedios_comun_m.map( (e,i) => i)
			,$scope.promedios_comun_m.map( e => e[e.length-1])
			,$scope.promedios_comun_m.map( (e,i) => 'AG' + i));
		var trace2 =  $scope.createTrace('Elites'
			,$scope.elites_comun_m.map( (e,i) => i)
			,$scope.elites_comun_m.map( e => e[e.length-1][0].fitness)
			,$scope.elites_comun_m.map( (e,i) => 'AG' + i)
			);
		var trace3 = $scope.createTrace('Promedios'
			,$scope.promedios_agpd_m.map( (e,i) => i)
			,$scope.promedios_agpd_m.map( e => e[e.length-1])
			,$scope.promedios_agpd_m.map( (e,i) => 'AG' + i));
		var trace4 = $scope.createTrace('Elites'
			,$scope.elites_agpd_m.map( (e,i) => i)
			,$scope.elites_agpd_m.map( e => e[e.length-1].fitness)
			,$scope.elites_agpd_m.map( (e,i) => 'AG' + i)
			);
		var trace5 = $scope.createTrace('Cantidad de Población'
			,$scope.poblaciones_agpd_m.map( (e,i) => i)
			,$scope.poblaciones_agpd_m.map( e => e.length)
			,$scope.poblaciones_agpd_m.map( (e,i) => 'AG' + i)
			);
		var tracesPoblaciones = [];
		$scope.poblaciones_agpd_m.forEach(
			(e,i) => tracesPoblaciones.push({
				x:Array(e.length).fill([i,i+1]),
				y:e.map( (e,i) => [i,i] ),
				z:e.map( (e,i) => [e.length,e.length] ),
				type: 'surface',
				showscale: false
			})
			);
		var layout = {
			title: 'Poblaciones',
			showlegend: false,
			autosize: true,
			scene: {
				xaxis: {title: 'Tiro'},
				yaxis: {title: 'Generación'},
				zaxis: {title: 'Cantidad de Individuos'}
			},
			margin: {
				t: 0, //top margin
				l: 0, //left margin
				r: 0, //right margin
				b: 0 //bottom margin
			},
		};
		Plotly.newPlot('multiples1', [trace1,trace2], {responsive: true});
		Plotly.newPlot('multiples2', [trace3,trace4], {responsive: true});
		Plotly.newPlot('poblacionesmultiples', tracesPoblaciones,layout,{responsive: true});
		/*
		var elites_comun_ordenados = $scope.elites_comun_m.map( (e) => e[e.length-1][0] );
		elites_comun_ordenados = elites_comun_ordenados.sort( (a,b) => a.fitness - b.fitness );
		var elites_agpd_ordenados = $scope.elites_agpd_m.map( e => e[e.length-1] );
		elites_agpd_ordenados = elites_agpd_ordenados.sort( (a,b) => a.fitness - b.fitness );
		var media_comun = elites_comun_ordenados.reduce( (ant,act) => ant + act.fitness , 0) / elites_comun_ordenados.length;
		var desviacion_estandar_comun = Math.sqrt(elites_comun_ordenados.reduce( (ant,act) => ant + Math.pow(act.fitness - media_comun,2) , 0) / elites_comun_ordenados.length	);
		var media_agpd = elites_agpd_ordenados.reduce( (ant,act) => ant + act.fitness , 0) / elites_agpd_ordenados.length;
		var desviacion_estandar_agpd = Math.sqrt(elites_agpd_ordenados.reduce( (ant,act) => ant + Math.pow(act.fitness - media_agpd,2) , 0) / elites_agpd_ordenados.length	);

		var barras_comun = [
		{
			x: elites_comun_ordenados.map( (e,i) => i ),
			y: elites_comun_ordenados.map( e => (e.fitness - media_comun) / desviacion_estandar_comun	  ),
			type: 'bar'
		}
		];
		var barras_agpd = [
		{
			x: elites_agpd_ordenados.map( (e,i) => i ),
			y: elites_agpd_ordenados.map( e => (e.fitness - media_agpd )/ desviacion_estandar_agpd	 ),
			type: 'bar'
		}
		];
		Plotly.newPlot('barras1', barras_comun);
		Plotly.newPlot('barras2', barras_agpd);
		*/
	}
}])